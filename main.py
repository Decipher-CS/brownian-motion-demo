from game_setup import start

print('Start brownian motion demo?')
should_start = input('press 1/y to start')
if should_start == '1' or should_start == 'y':
    start()
else:
    print('Quitting')
