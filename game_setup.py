# Basic pygame program to show brownian motion
import pygame
from sys import exit
from particle import Particle


def start():
    pygame.init()

    screen_height = 600
    screen_width = 600

    surface_height = screen_height
    surface_width = screen_width

    screen = pygame.display.set_mode((screen_width, screen_height))

    clock = pygame.time.Clock()

    boundry = pygame.Surface((surface_width, surface_width))
    boundry.fill('Black')

    p1 = Particle([surface_width, surface_height])

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                exit()
        screen.blit(boundry, (0, 0))
        boundry.fill('grey')

        pygame.draw.circle(screen, ('Black'),
                           p1.coordinates, p1.radius)
        p1.update_coordinates()
        if (p1.didCollide()):
            p1.change_direction()

        pygame.display.update()
        clock.tick(30)
