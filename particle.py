import random


class Particle:
    def __init__(self, surface_dimensions: list, velocity=15, radius=15):
        self.radius = radius
        self.surface_dimentions = surface_dimensions
        self.coordinates = [i/2 for i in surface_dimensions]
        self.velocity = velocity
        self.direction = [1, 1]

    def change_velocity(self, new_velocity: int):
        old_velocity = self.velocity
        self.velocity = new_velocity
        return old_velocity

    def update_coordinates(self):
        [x_coordinate, y_coordinate] = self.coordinates
        [x_dir, y_dir] = self.direction
        x_coordinate += (self.velocity * x_dir)
        y_coordinate += (self.velocity * y_dir)
        self.coordinates = [x_coordinate, y_coordinate]

    def didCollide(self):
        [x, y] = self.coordinates
        [surface_width, surface_height] = self.surface_dimentions
        radius = self.radius + 10

        if x <= radius or x >= surface_width-radius:
            return True
        if y <= radius or y >= surface_height-radius:
            return True
        return False

    def change_direction(self):
        [x_dir, y_dir] = self.direction
        rand_dir = Particle.get_random_direction()
        while (x_dir < 0 and rand_dir < 0) or (x_dir > 0 and rand_dir > 0):
            rand_dir = Particle.get_random_direction()
        x_dir = rand_dir
        rand_dir = Particle.get_random_direction()
        while (y_dir < 0 and rand_dir < 0) or (y_dir > 0 and rand_dir > 0):
            rand_dir = Particle.get_random_direction()
        y_dir = rand_dir
        self.direction = [x_dir, y_dir]

    @staticmethod
    def get_random_direction():
        return random.uniform(-1, 1)
